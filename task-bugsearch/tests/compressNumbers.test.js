const numberAsString = require('../compressNumbers');

describe('The compressNumbers function', () => {
    describe('Given a number input', () => {
        it('Compresses the numbers', () => {
            const res = numberAsString(12234);
            expect(res).toBe('11221314');
        });
        it('Compresses the numbers', () => {
            const res = numberAsString(0);
            expect(res).toBe('10');
        });
        it('Compresses the numbers', () => {
            const res = numberAsString(1e9);
            expect(res).toBe('1190');
        });
    });

    describe('When given wrong inputs', () => {
        it('returns error', () => {
            const res = numberAsString('a');
            expect(res).toBe('wrong input');
        });
        it('returns an error', () => {
            const res = numberAsString(null);
            expect(res).toBe('wrong input');
        });
        it('returns an error', () => {
            const res = numberAsString(undefined);
            expect(res).toBe('wrong input');
        });
    });
});
