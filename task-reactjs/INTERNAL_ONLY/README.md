## Assessments

-   Clear code
-   Following the requirements
-   Using best React practices
-   Implemented on typing timeout
-   Lift the state up for handling collapsed items
-   To include data-testid
