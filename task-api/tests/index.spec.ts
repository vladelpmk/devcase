import { expect } from "chai";
import { run } from "../index";

describe("Check implementation", function () {
  it("Is implemented", async function () {
    try {
      await run();
    } catch (error) {
      expect(error).to.be.null;
    }
  }).timeout(5000);

  it("Is returning correct values", async function () {
    expect(await run()).to.be.eq(41);
  }).timeout(5000);

  it("Is optimized", async function () {
    var start = +new Date();
    await run();
    var end = +new Date();
    expect(end - start).to.be.lessThan(2500);
  }).timeout(5000);
});
